// pobranie elementów
const _ = document;

var header = _.getElementById('header');
var anchors = _.getElementsByTagName('a');

// deklaracja nasłuchiwacza zdarzeń
_.addEventListener('scroll', handleScroll);

// tworzenie nowego elementu
var toTop = _.createElement('a');
toTop.innerHTML = "Na górę";
toTop.href = "#";

// dołączanie nowego elementu
var nav = _.querySelector('header#header>div');

function handleScroll() {
    if (_.documentElement.scrollTop > 10) {
        nav.appendChild(toTop);
        header.style.backgroundColor = 'bisque';
        for (i = 0; i < anchors.length; i++)
            anchors[i].style.color = 'black';
    } else {
        nav.removeChild(toTop);
        header.style.backgroundColor = 'white';
        for (i = 0; i < anchors.length; i++)
            anchors[i].style.color = 'blue';
    }
}