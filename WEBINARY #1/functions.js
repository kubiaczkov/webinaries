/**
 * Funkcja zmienia format imienia/nazwiska na poprawą formę.
 * 
 * @param {String} name Imię lub nazwisko.
 * @returns Poprawna forma.
 */
function fixedName(name) {
    return name[0].toUpperCase() + name.slice(1).toLowerCase();
}

console.log(fixedName("doMinIk") + ' ' + fixedName("kubiCa"));

/**
 * Funkcja łącząca podane argumenty tekstowe w jeden ciąg.
 * 
 * @returns {String} Połączony ciąg znaków.
 */
function inLineText() {
    const len = arguments.length;
    let result = '';
    for (let index = 0; index < len; index++) {
        result += `${arguments[index]} `;
    }
    return result;
}

// !dowolna ilość argumentów
console.log(inLineText("Ala", "ma", "kota")); // "Ala ma kota"

/**
 * Zwracanie różnej wartości w zależności od podanego parametru.
 * 
 * @param {Number} param Parametr numeryczny, domyślnie zero.
 * @returns {String} Wynik działania funkcji.
 */
function check(param = 0) {
    if (param > 0) {
        return "Parametr większy od zera";
    } else if (param < 0) {
        return "Parametr mniejszy od zera";
    } else if (param == 0) {
        return "Nie podano argumentu, przyjęto wartość domyślną";
    }
}

console.log(check(1)); // Parametr większy od zera
console.log(check(-1)); // Parametr mniejszy od zera
console.log(check()); // Nie podano argumentu, przyjęto wartość domyślną