// -------- OBIEKTY --------

/**
 * Deklaracja obiektu o nazwie `myFirstCar`, posiada on:
 * dwie właściwości `brand` oraz `color`
 * oraz jedną metodę `run`
 */
const myFirstCar = {
    // Właściwość określająca markę samochodu
    brand: "Volvo",
    // Właściwość opisująca kolor samochodu
    color: "Czerwone",
    /**
     * Metoda zwracająca przykładową informację
     */
    run() {
        return "Teraz jadę";
    },
    /**
     * Metoda wypisująca włąsciwośći samochodu
     */
    print() {
        return `${this.color} ${this.brand}`
    }
};

console.log(`Miałem ${myFirstCar.print()} ...`);
console.log(myFirstCar.run());