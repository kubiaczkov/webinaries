// -------- FUNKCJE STRZAŁKOWE --------

const classicSum = function (a, b) {
    return a + b;
};

const newSum = (a, b) => {
    return a + b;
}

const shortestSum = (a, b) => a + b;

console.log(classicSum(1, 2)); // 3
console.log(newSum(1, 2)); // 3 
console.log(shortestSum(1, 2)); // 3