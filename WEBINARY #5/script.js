var loop;

function clicked(img, src) {
    setTimeout(() => {
        document.getElementById('set').src = src;
    }, 1000);
    // nie czeka na wykonanie się timeoutu
    change(img, src);
}

function change(img, src) {
    img.src = src;
}

function roulette() {
    // pobranie elemntu do wyświetlania
    const show = document.getElementById('set');
    // kolekcja elementów typu img
    const images = document.images;
    // deklaracja zmiennych pomocniczych
    var index = 0;
    var next = images[index].src;
    const lastIndex = images.length; // 4

    loop = setInterval(() => {
        show.src = next;
        index++;
        if (index > lastIndex - 1)
            index = 0;
        next = images[index].src;
    }, 1000);
}

function stop() {
    clearInterval(loop);
    alert('Ruletka zatrzymana!');
}