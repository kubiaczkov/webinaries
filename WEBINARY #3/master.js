// ZDARZENIA (ang. events) JAVA SCRIPT

var objBtn = document.getElementById('pierwszy');

objBtn.addEventListener('click', foo);
objBtn.addEventListener('mouseleave', function () {
    console.warn('Myszka zjechała');
})
objBtn.addEventListener('mouseenter', function () {
    console.error('Myszka wjechała');
})

// objBtn.removeEventListener('click', foo);

function foo() {
    console.log('Click');
}

// cd..
var counter = 0;

function imgInfo(img) {
    // console.log(img.src);
    counter++;
    console.log(counter);
}

var reset = document.getElementById('reset');
reset.addEventListener('click', function () {
    counter = 0;
})

document.getElementById('off').addEventListener('click', () => {
    document.getElementsByTagName('img')[0].onmouseover = null;
})

// cd..

var input = document.getElementById('temp');
var p = document.getElementById('temp_');
const max = 10;

input.addEventListener('keyup', () => {
    let val = input.value.length;
    p.innerHTML = `Pozostało ${max - val} znaków`;
})