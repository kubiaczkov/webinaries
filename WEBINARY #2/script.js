/**
 * Funkcja modyfikująca CSS dla paragrafu
 * 
 * @param btn Obiekt wciśniętego przycisku
 */
function changeCSS(btn) {
    // pobranie parametrów
    var wielkosc = document.getElementById('procent').value;

    var e = document.getElementById('styl');
    var styl = e.options[e.selectedIndex].value;

    var color = btn.id;

    var p = document.getElementById('paragraf');

    // ustawienie stylów
    /**
     * * font-family -> fontFamily
     */
    p.style.fontSize = `${wielkosc}%`;
    p.style.fontStyle = `${styl}`;
    p.style.color = `${color}`;
}