// const form = document.forms[0];
// console.log(form);

function sub(form) {
    // przeprowadzic walidacje
    var valResult = val(form);

    if (valResult == true) {
        // jezeli poprawna
        alert('Data correct!');
        location.reload();
    } else {
        // niepoprawna
        alert('Validation error!');
        if (confirm('Reset form?')) {
            document.getElementById('reset').click();
            document.getElementById('name').focus();
        } else {
            for (let i = 0; i < form.elements.length; i++) {
                const element = form.elements[i];
                element.style.backgroundColor = 'White';
                element.title = '';
            }
            // walidacja niepoprawna
            // id - wiadomość
            for (i in valResult) {
                const el = document.getElementById(valResult[i][0]);
                el.style.backgroundColor = 'LightCoral';
                el.title = valResult[i][1];
            }
        }
    }

}

function val(form) {
    var err = [];
    // sprawdzanie elementów formularza
    for (let i = 0; i < form.elements.length; i++) {
        const element = form.elements[i];
        if (element.type != 'button' && element.type != 'checkbox')
            if (!element.checkValidity()) {
                let temp = [`${element.id}`, `${element.validationMessage}`];
                err.push(temp);
            }
    }
    // return
    if (err.length > 0)
        return err;
    else return true;
}